# [Semantic Web](http://www.engineeringarchitecture.unibo.it/en/programmes/course-unit-catalogue/course-unit/2015/377439) Course - 42500

- *Author*: Giacomo Mantani
- *Academic Year*: 2015/2016
- *License*: MIT (see `./LICENSE`)

## Project Description

This is a [NodeJS](https://nodejs.org/en/) driver for [dandelion](https://dandelion.eu/) Entity Extraction API. The purpose of this driver is extract information from a text or a URI in order to obtain usable, linked and semantic valuable TAGS.

### The Problem

Tags represent keywords used to describe a web resource. They have been
introduced by Del.icio.us to a wider audience in 2003, and soon after they
became an extremely popular way of organizing and sharing large collections of
data in online social communities. Compared to previous organization methods
(categories/folders), tags are simple to add, flexible, and can designate
membership in more than one category at the same time. When used collectively
they become a powerful categorization tool.

However, tags have some considerable disadvantages. They do not provide
information about their meaning, the semantics of the words tags may be
composed of. Polysemy (the same word can refer to different concepts), synonymy
(the same concept can be pointed out using different words), different lexical
forms (different noun forms, different verb conjugation, acronyms, different
languages), misspelling errors are some problems that arise when using tags.

For example, the tag ‘orange’ might refer to the fruit or the color, and the
different tags ‘movie’ and ‘film’ can be used to describe the same concept. This
lack of semantic distinction leads to inappropriate connections between items,
making them hard to search and browse through. Tags are essentially a full text
search mechanism, not graph linkages that machines can understand. Simply put,
the same reason that makes tags difficult to process also makes them so popular,
they are just random words.

The Linked Open Data effort provides semantically interlinked resources with
defined meaning. However, motivating people to use these resources for
publishing structured data is still a great challenge, as applications that
would allow them to do that are still overly complex and not user-friendly.

On the one hand there are flexible tags with little structure or semantics
widely accepted as a way of organizing information on the Web, and, on the other
hand, there exist highly formalized and complex semantic technologies and
standards struggling to find their way into the mainstream.

Finally, the problem is — how to add meaning to the tags, or, from the other
perspective, how to bring Web 2.0 practices to semantic technologies and make
them more flexible and easier to use.

### Backgrounds

I have made a presentation with Prezi about semantic web and how it can solve common TAGS problem.

**Video**: [Prezi.com - Giacomo Mantani WS Presentation](http://prezi.com/jnhuj9-fun7c/?utm_campaign=share&utm_medium=copy)

### About Dandelion
Context Intelligence: from text to actionable data. Extract meaning from unstructured text and put it in context with a simple API.
Thanks to its revolutionary technology, Dandelion API works well even on short and malformed texts in English, French, German, Italian and Portuguese.

## Project Details

### Dependencies

* [NodeJS](https://nodejs.org/en/)
* [npm](https://www.npmjs.com/)
* A _valid_ dandelion *API token*

### Steps

1. **Download** or **clone** the repository
2. Enter in the root directory `cd [DIR-REPOSITORY]`
3. Install all dependencies `npm install`
4. Create the file `config.json`, with this content:
	 ```
	 {
			"token":"HERE_API_TOKEN"
   }
	 ```
5. Run it! `node index.js --url=http://santafe.edu --uniq`

### Valid commands

`node index.js --help`

```
[Needed] -----------------------------------------------------------------------

  --url      Resource where extract informations from
  OR
  ...        Append words at the end of the command
             Like:
                node index.js --uniq Only the paranoid survive Andrew Grove

[Additional commands] ----------------------------------------------------------

  --wtitle   Print Wikipedia title
  --clab     Print Common label
  --types    Print Types
  --cat      Print Categories
  --lod      Print Linked resources
  --alt      Print Alternate labels
  --uniq     Print only uniq tags
  -f         Be as verbose as you can
```

## Key Benefits of Semantic Technology

- enabling a tag standardization by adding a meaning
- enabling auto-classification and multilingual semantic tagging
- facilitating information integration and knowledge discovery
- facilitating communication between different services

## Conclusion

Web 2.0 showed that it is possible to have successful systems based on
decentralized creation and collaboration of big online communities. Despite all
its disadvantages, tagging emerged as a (good enough) way to integrate and
organize the data. Semantic tags, as an intersection point of the two worlds,
have the potential to enable much faster evolution of the Web by providing a
solid foundation from which the Semantic Web can grow soundly.

Like [Faviki](www.faviki.com/) approach demonstrates a number of benefits compared to
classic social bookmarking systems, this **nodeJS** **dandelion** driver help a
user or a web services to exploit it full potential.

## From now on, tests follow

### Examples

#### --uniq

Command:

`node index.js --url https://cs.brown.edu/~sk/ --uniq`

Output:

```
[Query]
api.dandelion.eu/datatxt/nex/v1/?url=https://cs.brown.edu/~sk/&include=types%2Ccategories%2Clod%2Calternate_labels&token=e7c1551c1cd84db68ecd29dea29834c0

[Tags]
shriramkrishnamurthi computersecurity computernetwork verificationandvalidation humancomputerinteraction programminglanguage software javascript worldwideweb racket flapjax blog bootstrap unitedstates computer mathematicseducation middleschool codeorg education brownuniversity henrywriston research sigplan robinmilner corporation nationalsciencefoundation cisco europeanspaceagency fujitsu google infosys janestreetcapital rhodeisland tripadvisor judgeforyourselves jiddukrishnamurti philosopher searchengine
```
#### --wtitle
Command:

`node index.js --url https://cs.brown.edu/~sk/ --wtitle`

Output:

```
[Query]
api.dandelion.eu/datatxt/nex/v1/?url=https://cs.brown.edu/~sk/&include=types%2Ccategories%2Clod%2Calternate_labels&token=e7c1551c1cd84db68ecd29dea29834c0
[Wikipedia title ]
Shriram Krishnamurthi
[Wikipedia title ]
Computer security
[Wikipedia title ]
Computer network
[Wikipedia title ]
Verification and validation
[Wikipedia title ]
Human–computer interaction
[Wikipedia title ]
Programming language
[Wikipedia title ]
Software
[Wikipedia title ]
JavaScript
[Wikipedia title ]
World Wide Web
[Wikipedia title ]
Racket (programming language)
[Wikipedia title ]
Flapjax
[Wikipedia title ]
Blog
[Wikipedia title ]
Programming language
[Wikipedia title ]
Bootstrapping (statistics)
[Wikipedia title ]
United States
[Wikipedia title ]
Computer
[Wikipedia title ]
Mathematics education
[Wikipedia title ]
Middle school
[Wikipedia title ]
Mathematics education
[Wikipedia title ]
Code.org
[Wikipedia title ]
Education
[Wikipedia title ]
Brown University
[Wikipedia title ]
Henry Wriston
[Wikipedia title ]
Research
[Wikipedia title ]
SIGPLAN
[Wikipedia title ]
Robin Milner
[Wikipedia title ]
Research
[Wikipedia title ]
Corporation
[Wikipedia title ]
National Science Foundation
[Wikipedia title ]
Cisco Systems
[Wikipedia title ]
European Space Agency
[Wikipedia title ]
Fujitsu
[Wikipedia title ]
Google
[Wikipedia title ]
Infosys
[Wikipedia title ]
Jane Street Capital
[Wikipedia title ]
Rhode Island
[Wikipedia title ]
TripAdvisor
[Wikipedia title ]
Judge for Yourselves!
[Wikipedia title ]
Jiddu Krishnamurti
[Wikipedia title ]
Philosopher
[Wikipedia title ]
Web search engine
```
#### --clab

Command:

`node index.js --url https://cs.brown.edu/~sk/ --clab`

Output:

```
[Query]
api.dandelion.eu/datatxt/nex/v1/?url=https://cs.brown.edu/~sk/&include=types%2Ccategories%2Clod%2Calternate_labels&token=e7c1551c1cd84db68ecd29dea29834c0
[Common label    ]
Shriram Krishnamurthi
[Common label    ]
Computer security
[Common label    ]
Computer network
[Common label    ]
Verification and validation
[Common label    ]
Human-computer interaction
[Common label    ]
Programming language
[Common label    ]
Software
[Common label    ]
JavaScript
[Common label    ]
World Wide Web
[Common label    ]
Racket
[Common label    ]
Flapjax
[Common label    ]
Blog
[Common label    ]
Programming language
[Common label    ]
Bootstrap
[Common label    ]
United States
[Common label    ]
Computer
[Common label    ]
Mathematics education
[Common label    ]
Middle school
[Common label    ]
Mathematics education
[Common label    ]
Code.org
[Common label    ]
Education
[Common label    ]
Brown University
[Common label    ]
Henry Wriston
[Common label    ]
Research
[Common label    ]
SIGPLAN
[Common label    ]
Robin Milner
[Common label    ]
Research
[Common label    ]
Corporation
[Common label    ]
National Science Foundation
[Common label    ]
Cisco
[Common label    ]
European Space Agency
[Common label    ]
Fujitsu
[Common label    ]
Google
[Common label    ]
Infosys
[Common label    ]
Jane Street Capital
[Common label    ]
Rhode Island
[Common label    ]
TripAdvisor
[Common label    ]
Judge for Yourselves!
[Common label    ]
Jiddu Krishnamurti
[Common label    ]
Philosopher
[Common label    ]
Search engine
```

#### --types

Command:

`node index.js --url https://cs.brown.edu/~sk/ --types`

Output:

```
[Query]
api.dandelion.eu/datatxt/nex/v1/?url=https://cs.brown.edu/~sk/&include=types%2Ccategories%2Clod%2Calternate_labels&token=e7c1551c1cd84db68ecd29dea29834c0
[Types           ]
[]
[Types           ]
[]
[Types           ]
[]
[Types           ]
[]
[Types           ]
[]
[Types           ]
[]
[Types           ]
[]
[Types           ]
[ 'http://dbpedia.org/ontology/ProgrammingLanguage' ]
[Types           ]
[]
[Types           ]
[ 'http://dbpedia.org/ontology/Software',
  'http://dbpedia.org/ontology/Work',
  'http://dbpedia.org/ontology/ProgrammingLanguage' ]
[Types           ]
[]
[Types           ]
[]
[Types           ]
[]
[Types           ]
[]
[Types           ]
[ 'http://dbpedia.org/ontology/Place',
  'http://dbpedia.org/ontology/PopulatedPlace',
  'http://dbpedia.org/ontology/Country' ]
[Types           ]
[]
[Types           ]
[]
[Types           ]
[]
[Types           ]
[]
[Types           ]
[]
[Types           ]
[]
[Types           ]
[ 'http://dbpedia.org/ontology/EducationalInstitution',
  'http://dbpedia.org/ontology/Agent',
  'http://dbpedia.org/ontology/Organisation',
  'http://dbpedia.org/ontology/University' ]
[Types           ]
[]
[Types           ]
[]
[Types           ]
[ 'http://dbpedia.org/ontology/Agent' ]
[Types           ]
[ 'http://dbpedia.org/ontology/Scientist',
  'http://dbpedia.org/ontology/Agent',
  'http://dbpedia.org/ontology/Person' ]
[Types           ]
[]
[Types           ]
[]
[Types           ]
[ 'http://dbpedia.org/ontology/GovernmentAgency',
  'http://dbpedia.org/ontology/Agent',
  'http://dbpedia.org/ontology/Organisation' ]
[Types           ]
[ 'http://dbpedia.org/ontology/Agent',
  'http://dbpedia.org/ontology/Organisation',
  'http://dbpedia.org/ontology/Company' ]
[Types           ]
[ 'http://dbpedia.org/ontology/Agent',
  'http://dbpedia.org/ontology/Organisation' ]
[Types           ]
[ 'http://dbpedia.org/ontology/Agent',
  'http://dbpedia.org/ontology/Organisation',
  'http://dbpedia.org/ontology/Company' ]
[Types           ]
[ 'http://dbpedia.org/ontology/Agent',
  'http://dbpedia.org/ontology/Organisation',
  'http://dbpedia.org/ontology/Company' ]
[Types           ]
[ 'http://dbpedia.org/ontology/Agent',
  'http://dbpedia.org/ontology/Organisation',
  'http://dbpedia.org/ontology/Company' ]
[Types           ]
[]
[Types           ]
[ 'http://dbpedia.org/ontology/Place',
  'http://dbpedia.org/ontology/PopulatedPlace',
  'http://dbpedia.org/ontology/AdministrativeRegion' ]
[Types           ]
[ 'http://dbpedia.org/ontology/Agent',
  'http://dbpedia.org/ontology/Organisation',
  'http://dbpedia.org/ontology/Company' ]
[Types           ]
[]
[Types           ]
[ 'http://dbpedia.org/ontology/Agent',
  'http://dbpedia.org/ontology/Person' ]
[Types           ]
[ 'http://dbpedia.org/ontology/Agent',
  'http://dbpedia.org/ontology/Person' ]
[Types           ]
[]
```

#### --cat

Command:

`node index.js --url https://cs.brown.edu/~sk/ --cat`

Output:

```
[Query]
api.dandelion.eu/datatxt/nex/v1/?url=https://cs.brown.edu/~sk/&include=types%2Ccategories%2Clod%2Calternate_labels&token=e7c1551c1cd84db68ecd29dea29834c0
[Categories      ]
[ 'Programming language researchers', 'Rice University alumni' ]
[Categories      ]
[ 'Computer security',
  'E-commerce',
  'Secure communication',
  'Computer network security',
  'Crime prevention',
  'National security' ]
[Categories      ]
[ 'Computer networks',
  'Computer networking',
  'Telecommunications engineering' ]
[Categories      ]
[ 'Quality management',
  'Systems engineering',
  'Pharmaceutical industry',
  'Food safety',
  'Validity (statistics)' ]
[Categories      ]
[ 'Human communication',
  'Human–computer interaction',
  'Human–machine interaction',
  'Scientific revolution',
  'Behavioral and social facets of systemic risk' ]
[Categories      ]
[ 'Programming language topics', 'Notation' ]
[Categories      ]
[ 'Software' ]
[Categories      ]
[ '1995 introductions',
  'American inventions',
  'Cross-platform software',
  'Functional languages',
  'JavaScript',
  'Object-based programming languages',
  'Programming languages created in 1995',
  'Prototype-based programming languages',
  'Scripting languages',
  'Web programming' ]
[Categories      ]
[ 'World Wide Web',
  '1989 introductions',
  'English inventions',
  'Human–computer interaction',
  'Information Age' ]
[Categories      ]
[ 'Functional languages',
  'Object-oriented programming languages',
  'Extensible syntax programming languages',
  'Lisp programming language family',
  'Scheme implementations',
  'Scheme compilers',
  'Scheme interpreters',
  'R6RS Scheme',
  'Academic programming languages',
  'Educational programming languages',
  'Pedagogic integrated development environments',
  'Cross-platform free software',
  'Free compilers and interpreters',
  'Programming languages created in 1994',
  'Articles with example Racket code' ]
[Categories      ]
[ 'Scripting languages', 'Reactive programming languages' ]
[Categories      ]
[ 'Blogs',
  'Internet terminology',
  'Literary genres',
  'Words coined in the 1990s' ]
[Categories      ]
[ 'Programming language topics', 'Notation' ]
[Categories      ]
[ 'Computational statistics',
  'Data analysis',
  'Statistical inference',
  'Resampling (statistics)' ]
[Categories      ]
[ '1776 establishments in the United States',
  'Article Feedback 5 Additional Articles',
  'G8 nations',
  'G20 nations',
  'Liberal democracies',
  'Member states of the United Nations',
  'Republics',
  'United States',
  'States and territories established in 1776',
  'Former British colonies',
  'Superpowers' ]
[Categories      ]
[ 'Computers' ]
[Categories      ]
[ 'Articles with inconsistent citation formats',
  'Mathematical science occupations',
  'Mathematics education' ]
[Categories      ]
[ 'School types',
  'Educational stages',
  'Pedagogy',
  'High schools and secondary schools',
  'School terminology',
  'Youth',
  'Elementary and primary schools' ]
[Categories      ]
[ 'Articles with inconsistent citation formats',
  'Mathematical science occupations',
  'Mathematics education' ]
[Categories      ]
undefined
[Categories      ]
[ 'Education', 'Knowledge sharing', 'Philosophy of education' ]
[Categories      ]
[ 'Brown University',
  'Association of American Universities',
  'Colonial architecture in Rhode Island',
  'Colonial Colleges',
  'Educational institutions established in the 1760s',
  'Georgian architecture in Rhode Island',
  'National Association of Independent Colleges and Universities members',
  'New England Association of Schools and Colleges',
  'Non-profit organizations based in Rhode Island',
  'Rhode Island in the American Revolution',
  'Universities and colleges in Providence, Rhode Island',
  '1764 establishments in Rhode Island',
  '1760s in the United States' ]
[Categories      ]
[ '1889 births',
  '1978 deaths',
  'American educationists',
  'American educators',
  'American non-fiction writers',
  'Harvard University alumni',
  'Presidents of Brown University',
  'Presidents of Lawrence University',
  'Wesleyan University alumni' ]
[Categories      ]
[ 'Research', 'Research methods' ]
[Categories      ]
[ 'Association for Computing Machinery Special Interest Groups' ]
[Categories      ]
[ '1934 births',
  '2010 deaths',
  'People from Devon',
  'People educated at Eton College',
  'Alumni of King\'s College, Cambridge',
  'British computer scientists',
  'Fellows of the Royal Society',
  'Turing Award laureates',
  'Academics of City University London',
  'Academics of Swansea University',
  'Stanford University School of Engineering faculty',
  'Academics of the University of Edinburgh',
  'Formal methods people',
  'Members of the University of Cambridge Computer Laboratory',
  'Fellows of the Association for Computing Machinery',
  'Programming language designers',
  'Programming language researchers',
  'Fellows of the Royal Society of Edinburgh',
  'Fellows of the British Computer Society',
  'Royal Engineers officers',
  'Computer science writers',
  'Deaths from myocardial infarction',
  'Cardiovascular disease deaths in England',
  'Members of IFIP Technical Committee 1' ]
[Categories      ]
[ 'Research', 'Research methods' ]
[Categories      ]
[ 'Companies',
  'Business law',
  'Corporations law',
  'Corporations',
  'Legal entities',
  'Types of business entity' ]
[Categories      ]
[ 'Independent agencies of the United States government',
  'National Science Foundation',
  'Foundations based in the United States',
  'Science and technology in the United States',
  'Organizations established in 1950',
  '1950 establishments in the United States',
  'Funding bodies' ]
[Categories      ]
[ '1994 establishments in the United States',
  'Cisco Systems',
  'Companies based in San Jose, California',
  'Companies established in 1984',
  'Companies in the Dow Jones Industrial Average',
  'Companies in the NASDAQ-100 Index',
  'Deep packet inspection',
  'Multinational companies headquartered in the United States',
  'Networking companies of the United States',
  'Networking hardware companies',
  'Telecommunications equipment vendors',
  'Videotelephony',
  'Companies listed on NASDAQ',
  'Technology companies based in the San Francisco Bay Area' ]
[Categories      ]
[ 'European Space Agency',
  'Space agencies',
  'Space organizations',
  'Organizations established in 1975',
  'Science and technology in Europe',
  'Organizations based in Paris' ]
[Categories      ]
[ 'Cloud computing providers',
  'Companies based in Tokyo',
  'Companies of Japan',
  'Computer hardware companies',
  'Electronics companies of Japan',
  'Display technology companies',
  'Companies established in 1935',
  'Companies listed on the Tokyo Stock Exchange',
  'Software companies of Japan',
  'Fujitsu',
  'Computer storage companies',
  'Semiconductor companies',
  'Netbook manufacturers',
  'HVAC manufacturing companies',
  'Telecommunications companies of Japan',
  '1935 establishments in Japan' ]
[Categories      ]
[ 'Google',
  '1998 establishments in the United States',
  'Article Feedback 5 Additional Articles',
  'Cloud computing providers',
  'Companies based in Mountain View, California',
  'Companies established in 1998',
  'Companies listed on NASDAQ',
  'Human–computer interaction',
  'Internet advertising',
  'Internet companies of the United States',
  'Internet properties established in 1998',
  'Multinational companies headquartered in the United States',
  'Web service providers',
  'Websites by company',
  'World Wide Web' ]
[Categories      ]
[ 'BSE SENSEX',
  'Companies established in 1981',
  'Software companies of India',
  'Companies listed on the Bombay Stock Exchange',
  'Outsourcing companies',
  'Information technology companies of Bangalore',
  'Companies based in Bangalore',
  'International information technology consulting firms',
  'Information technology consulting firms of India',
  'Infosys',
  'Multinational companies headquartered in India',
  'Companies listed on the New York Stock Exchange',
  'S&P CNX Nifty' ]
[Categories      ]
undefined
[Categories      ]
[ 'Rhode Island',
  'East Coast of the United States',
  'Former British colonies',
  'Former English colonies',
  'New England',
  'Northeastern United States',
  'Populated places in the United States with Italian-American plurality populations',
  'States and territories established in 1663',
  'States of the United States',
  '1663 establishments in the Thirteen Colonies' ]
[Categories      ]
[ 'Companies established in 2000',
  'Companies based in Middlesex County, Massachusetts',
  'Software companies based in Massachusetts',
  'Consumer guides',
  'Newton, Massachusetts',
  'Travel websites' ]
[Categories      ]
[ '1876 books', 'Books by Søren Kierkegaard' ]
[Categories      ]
[ '1895 births',
  '1986 deaths',
  '20th-century philosophers',
  'Contemporary Indian philosophers',
  'Deaths from pancreatic cancer',
  'Hindu philosophers',
  'Indian spiritual writers',
  'New Age spiritual leaders',
  'New Age writers',
  'Telugu people',
  'Theosophy',
  'Indian spiritual teachers',
  'Founders of Indian schools and colleges',
  'Bodhisattvas' ]
[Categories      ]
[ 'Humanities occupations', 'Philosophers' ]
[Categories      ]
[ 'Information retrieval',
  'Internet search engines',
  'Internet terminology',
  'History of the Internet' ]
```

#### --lod

Command:

`node index.js --url https://cs.brown.edu/~sk/ --lod`

Output:

```
[Query]
api.dandelion.eu/datatxt/nex/v1/?url=https://cs.brown.edu/~sk/&include=types%2Ccategories%2Clod%2Calternate_labels&token=e7c1551c1cd84db68ecd29dea29834c0
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Shriram_Krishnamurthi',
  dbpedia: 'http://dbpedia.org/resource/Shriram_Krishnamurthi' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Computer_security',
  dbpedia: 'http://dbpedia.org/resource/Computer_security' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Computer_network',
  dbpedia: 'http://dbpedia.org/resource/Computer_network' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Verification_and_validation',
  dbpedia: 'http://dbpedia.org/resource/Verification_and_validation' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Human%E2%80%93computer_interaction',
  dbpedia: 'http://dbpedia.org/resource/Human%E2%80%93computer_interaction' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Programming_language',
  dbpedia: 'http://dbpedia.org/resource/Programming_language' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Software',
  dbpedia: 'http://dbpedia.org/resource/Software' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/JavaScript',
  dbpedia: 'http://dbpedia.org/resource/JavaScript' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/World_Wide_Web',
  dbpedia: 'http://dbpedia.org/resource/World_Wide_Web' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Racket_%28programming_language%29',
  dbpedia: 'http://dbpedia.org/resource/Racket_(programming_language)' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Flapjax',
  dbpedia: 'http://dbpedia.org/resource/Flapjax' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Blog',
  dbpedia: 'http://dbpedia.org/resource/Blog' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Programming_language',
  dbpedia: 'http://dbpedia.org/resource/Programming_language' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Bootstrapping_%28statistics%29',
  dbpedia: 'http://dbpedia.org/resource/Bootstrapping_(statistics)' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/United_States',
  dbpedia: 'http://dbpedia.org/resource/United_States' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Computer',
  dbpedia: 'http://dbpedia.org/resource/Computer' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Mathematics_education',
  dbpedia: 'http://dbpedia.org/resource/Mathematics_education' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Middle_school',
  dbpedia: 'http://dbpedia.org/resource/Middle_school' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Mathematics_education',
  dbpedia: 'http://dbpedia.org/resource/Mathematics_education' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Code.org',
  dbpedia: 'http://dbpedia.org/resource/Code.org' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Education',
  dbpedia: 'http://dbpedia.org/resource/Education' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Brown_University',
  dbpedia: 'http://dbpedia.org/resource/Brown_University' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Henry_Wriston',
  dbpedia: 'http://dbpedia.org/resource/Henry_Wriston' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Research',
  dbpedia: 'http://dbpedia.org/resource/Research' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/SIGPLAN',
  dbpedia: 'http://dbpedia.org/resource/SIGPLAN' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Robin_Milner',
  dbpedia: 'http://dbpedia.org/resource/Robin_Milner' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Research',
  dbpedia: 'http://dbpedia.org/resource/Research' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Corporation',
  dbpedia: 'http://dbpedia.org/resource/Corporation' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/National_Science_Foundation',
  dbpedia: 'http://dbpedia.org/resource/National_Science_Foundation' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Cisco_Systems',
  dbpedia: 'http://dbpedia.org/resource/Cisco_Systems' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/European_Space_Agency',
  dbpedia: 'http://dbpedia.org/resource/European_Space_Agency' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Fujitsu',
  dbpedia: 'http://dbpedia.org/resource/Fujitsu' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Google',
  dbpedia: 'http://dbpedia.org/resource/Google' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Infosys',
  dbpedia: 'http://dbpedia.org/resource/Infosys' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Jane_Street_Capital',
  dbpedia: 'http://dbpedia.org/resource/Jane_Street_Capital' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Rhode_Island',
  dbpedia: 'http://dbpedia.org/resource/Rhode_Island' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/TripAdvisor',
  dbpedia: 'http://dbpedia.org/resource/TripAdvisor' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Judge_for_Yourselves%21',
  dbpedia: 'http://dbpedia.org/resource/Judge_for_Yourselves!' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Jiddu_Krishnamurti',
  dbpedia: 'http://dbpedia.org/resource/Jiddu_Krishnamurti' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Philosopher',
  dbpedia: 'http://dbpedia.org/resource/Philosopher' }
[Linked resources]
{ wikipedia: 'http://en.wikipedia.org/wiki/Web_search_engine',
  dbpedia: 'http://dbpedia.org/resource/Web_search_engine' }
```

#### --alt

Command:

`node index.js --url https://cs.brown.edu/~sk/ --alt`

Output:

```
[Query]
api.dandelion.eu/datatxt/nex/v1/?url=https://cs.brown.edu/~sk/&include=types%2Ccategories%2Clod%2Calternate_labels&token=e7c1551c1cd84db68ecd29dea29834c0
[Alternate labels]
[ 'Shriram Krishnamurthi' ]
[Alternate labels]
[ 'Computer security',
  'Security',
  'Cyber security',
  'Cybersecurity',
  'Computer insecurity' ]
[Alternate labels]
[ 'Computer network',
  'Network',
  'Computer networking',
  'Networking',
  'Networks' ]
[Alternate labels]
[ 'Verification and validation',
  'Verification',
  'Validation',
  'Validation and verification',
  'Verified' ]
[Alternate labels]
[ 'Human-computer interaction',
  'Human-computer interface',
  'Human-machine interaction',
  'HCI',
  'Computer-human interaction' ]
[Alternate labels]
[ 'Programming language',
  'Programming languages',
  'Computer language',
  'Language',
  'Programming' ]
[Alternate labels]
[ 'Software',
  'Computer software',
  'Software technology',
  'Scientific software',
  'Software product' ]
[Alternate labels]
[ 'JavaScript',
  'Server-side JavaScript',
  'Client-side JavaScript',
  'JS',
  'Live-Script' ]
[Alternate labels]
[ 'World Wide Web', 'Web', 'Web-based', 'WWW', 'Worldwide web' ]
[Alternate labels]
[ 'Racket', 'DrRacket', 'PLT', 'MzScheme', 'PLT Scheme' ]
[Alternate labels]
[ 'Flapjax' ]
[Alternate labels]
[ 'Blog', 'Blogs', 'Weblog', 'Blogging', 'Bloggers' ]
[Alternate labels]
[ 'Programming language',
  'Programming languages',
  'Computer language',
  'Language',
  'Programming' ]
[Alternate labels]
[ 'Bootstrap',
  'Bootstrapping',
  'Bootstrap support',
  'Bootstrap sampling' ]
[Alternate labels]
[ 'United States',
  'American',
  'USA',
  'U.S.',
  'United States of America' ]
[Alternate labels]
[ 'Computer',
  'Computers',
  'Computer system',
  'Digital computer',
  'Computer systems' ]
[Alternate labels]
[ 'Mathematics education',
  'Mathematics',
  'Mathematical education',
  'Faculty of Mathematics',
  'Math teacher' ]
[Alternate labels]
[ 'Middle school',
  'Junior high school',
  'Junior high',
  'Middle',
  'Middle schools' ]
[Alternate labels]
[ 'Mathematics education',
  'Mathematics',
  'Mathematical education',
  'Faculty of Mathematics',
  'Math teacher' ]
[Alternate labels]
[ 'Code.org', 'Hour of Code' ]
[Alternate labels]
[ 'Education',
  'Teaching',
  'Educational',
  'Educationist',
  'Educationalist' ]
[Alternate labels]
[ 'Brown University',
  'Brown',
  'College in the English Colony of Rhode Island and Providence Plantations',
  'College of Rhode Island and Providence Plantations',
  'Brown College' ]
[Alternate labels]
[ 'Henry Wriston', 'Henry Merritt Wriston' ]
[Alternate labels]
[ 'Research',
  'Researcher',
  'Researchers',
  'Original research',
  'Study' ]
[Alternate labels]
[ 'SIGPLAN',
  'SIGPLAN Notices',
  'ACM SIGPLAN Notices',
  'SIGPLAN Programming Languages Software Award' ]
[Alternate labels]
[ 'Robin Milner' ]
[Alternate labels]
[ 'Research',
  'Researcher',
  'Researchers',
  'Original research',
  'Study' ]
[Alternate labels]
[ 'Corporation',
  'Corporate',
  'Corporations',
  'Company',
  'Incorporated' ]
[Alternate labels]
[ 'National Science Foundation',
  'NSF',
  'U.S. National Science Foundation',
  'National Science Foundation\'s',
  'National Science Foundation Act' ]
[Alternate labels]
[ 'Cisco',
  'Cisco Systems',
  'Cisco Systems, Inc.',
  'Cisco\'s',
  'Cisco 7600' ]
[Alternate labels]
[ 'European Space Agency',
  'ESA',
  'ESA\'s',
  'European',
  'European Space Agency\'s' ]
[Alternate labels]
[ 'Fujitsu',
  'Fujitsu Limited',
  'Fujitsu Laboratories Ltd',
  'Fujitsu Services',
  'Fujitsu Ltd.' ]
[Alternate labels]
[ 'Google',
  'Google Inc.',
  'Google\'s',
  'Author Rank',
  'Google Guys' ]
[Alternate labels]
[ 'Infosys',
  'Infosys Technologies',
  'Infosys Limited',
  'Infosys Technologies Ltd.',
  'Infosys Ltd' ]
[Alternate labels]
[ 'Jane Street Capital' ]
[Alternate labels]
[ 'Rhode Island',
  'RI',
  'State of Rhode Island and Providence Plantations',
  'State of Rhode Island',
  'Rhode Island and Providence Plantations' ]
[Alternate labels]
[ 'TripAdvisor', 'Trip Advisor', 'TripAdvisor.com' ]
[Alternate labels]
[ 'Judge for Yourselves!', 'Judge for Yourself!' ]
[Alternate labels]
[ 'Jiddu Krishnamurti', 'J. Krishnamurti', 'Krishnamurti' ]
[Alternate labels]
[ 'Philosopher', 'Philosophers', 'Sage' ]
[Alternate labels]
[ 'Search engine',
  'Web search engine',
  'Search engines',
  'Search',
  'Web search' ]
```

