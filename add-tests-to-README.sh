#!/bin/env sh
commands="--uniq --wtitle --clab --types --cat --lod --alt"
for cmd in $commands; do
  echo "#### Example $cmd" >> README.md
  echo "Command \`node index.js --url https://cs.brown.edu/~sk/ $cmd\`" >> README.md
  echo '```' >> README.md
  node index.js --url https://cs.brown.edu/~sk/ $cmd >> README.md
  echo '```' >> README.md
done

