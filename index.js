// Author: Giacomo Mantani
// License: MIT
//
// See the api docs at:
// https://dandelion.eu/docs/api/datatxt/nex/v1/

var http = require('http'),
    parseArgs = require('minimist'),
    config  = require('./config');

var argv = parseArgs(process.argv.slice(2));

var options = {
  host: 'api.dandelion.eu',
  path: '/datatxt/nex/v1/?'						     // endpoint

  //headers: {'Connection': 'keep-alive'}  // suggested if multiple request
};

if (argv.help || argv.h) {
  console.log(`
              Needed:

                --url      Resource where extract informations from
                OR

                ...        Append words at the end of the command
                           Like: PROGRAM [COMMANDS] Only the paranoid survive Andrew Grove

              Additional commands:

                --wtitle   Print Wikipedia title
                --clab     Print Common label
                --types    Print Types
                --cat      Print Categories
                --lod      Print Linked resources
                --alt      Print Alternate labels
                --uniq     Print only uniq tags
                -f         Be as verbose as you can
              `);
  process.exit();
}

if(argv.url){
  options.path += "url=" + argv.url;
}
else{
  options.path += "text=" + encodeURIComponent(argv._);
}

options.path += "&include=" + encodeURIComponent("types,categories,lod,alternate_labels");
options.path += "&token=" + config.token;

console.log("[Query]");
console.log(options.host + options.path)

function pprintAnnotations(jsonobj) {
    if(argv.wtitle || argv.f) {
      console.log("[Wikipedia title ]");
      console.log(jsonobj.title);
    }
    if(argv.clab   || argv.f) {
      console.log("[Common label    ]");
      console.log(jsonobj.label);
    }
    if(argv.types  || argv.f) {
      console.log("[Types           ]");
      console.log(jsonobj.types);
    }
    if(argv.cat    || argv.f) {
      console.log("[Categories      ]");
      console.log(jsonobj.categories);
    }
    if(argv.lod    || argv.f) {
      console.log("[Linked resources]");
      console.log(jsonobj.lod);
    }
    if(argv.alt    || argv.f) {
      console.log("[Alternate labels]");
      console.log(jsonobj.alternateLabels);
    }
}

var req = http.request(options, function(response) {
  var str = ''
  response.on('data', function (chunk) {
    str += chunk;
  });

  response.on('end', function () {
    var json_response = JSON.parse(str);
    if(argv.f){
      console.log("[Annotated text]");
      console.log(json_response.text);
    }
    if(!argv.uniq){
      json_response.annotations.forEach(pprintAnnotations);
    }else{
      var uniq = Array.from(new Set(json_response.annotations.map(a => a.label)));
      var uniq_alpha = uniq.map(u => u.replace(/\W/g,""));
      var uniq_alpha_low = uniq_alpha.map(ua => ua.toLowerCase());
      console.log("\n[Tags]");
      console.log(uniq_alpha_low.join(" "));
    }
  });
}).end();
